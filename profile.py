"""
This is the simplest profile; a single Xen VM.  This profile can be instantiated on any ProtoGENI cluster; the node will boot the default operating system, which it typically a recent version of Ubuntu.

Instructions:
Click on the node in the topology and choose the shell menu item. When your shell window appears, use `sudo` to poke around.
"""

import geni.portal as portal
import geni.rspec.pg as pg
import geni.rspec.igext as igext

#
# This geni-lib script is designed to run in the Portal.
#
pc = portal.Context()

#
# Create our in-memory model of the RSpec -- the resources we're going to request
# in our experiment, and their configuration.
#
rspec = pg.Request()

#
# Create a single VM running the default operating system.
#
rspec.XenVM( "mynode" )

# This must be the last statement.
pc.printRequestRSpec( rspec )
